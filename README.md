# SmartMirror Dashboard

### Tile Ideas: 
- [ ] Weather w/ graph for temp/humidity/rain-precipitation during the day
- [ ] YouTube widget
- [ ] Countdowns for random shit
- [ ] Time widget w/ date
- [ ] Calendar
- [ ] Slideshow (last n google-photos img's)
- [ ] Soundcloud / Spotify
- [ ] Medium (medium.com, real good content) via RSS
- [ ] Twitter (Last n items from timeline)
- [ ] Amazon tracking for ordered packages

### General Concepts:
- The whole thing should be configurable via web interface.
- Some tiles should be Static, but others should be conditional
  (Amazon tracking only if package ordered, etc.)
- Global Grid system to make configuring easy
